import 'package:flutter/material.dart';
import 'package:cart_shooper/screens/cart_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
      //home: HomeScreen(),
      home: CartScreen(),
    );
  }
}
