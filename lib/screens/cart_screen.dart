import 'package:cart_shooper/widgets/cart_item_samples.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onDoubleTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back,
                        size: 25,
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          "Keranjang",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(width: 235),
                        InkWell(
                          onDoubleTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            CupertinoIcons.checkmark_square,
                            size: 22,
                          ),
                        ),
                        SizedBox(width: 4),
                        InkWell(
                          onDoubleTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.delete,
                            size: 22,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(25),
                child: Row(
                  children: [
                    RichText(
                        text: TextSpan(children: [
                      WidgetSpan(
                        child: Icon(Icons.pin_drop_outlined, size: 16),
                      ),
                    ])),
                    Text(
                      " Dikirim ke lab 3d smk raden umar said",
                      style: TextStyle(
                          fontSize: 16, fontWeight: FontWeight.normal),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 15),
                child: Column(
                  children: [
                    CartItemSamples(),
                    //SizedBox(height: 50),
                    //SizedBox(height: 15),
                    SizedBox(height: 160),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => CartScreen(),
                            ));
                      },
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 30),
                                  child: FloatingActionButton.extended(
                                    onPressed: () {
                                      showModalBottomSheet(
                                        isScrollControlled: true,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.vertical(
                                          top: Radius.circular(20),
                                        )),
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Scaffold(
                                            appBar: new AppBar(
                                                // title: new Text("test"),
                                                ),
                                            body: new Container(
                                              child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(20),
                                                  child: Column(
                                                    children: [
                                                      TextField(
                                                        showCursor: true,
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              OutlineInputBorder(),
                                                          hintText:
                                                              'Masukkan Nama Barang',
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 18,
                                                      ),
                                                      TextField(
                                                        showCursor: true,
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              OutlineInputBorder(),
                                                          hintText:
                                                              'Masukan Deskripsi Barang',
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 18,
                                                      ),
                                                      TextField(
                                                        showCursor: true,
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              OutlineInputBorder(),
                                                          hintText:
                                                              'Pilih Kategori',
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 18,
                                                      ),
                                                      TextField(
                                                        showCursor: true,
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              OutlineInputBorder(),
                                                          hintText:
                                                              'Masukan Jumlah Barang',
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 18,
                                                      ),
                                                      TextField(
                                                        showCursor: true,
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              OutlineInputBorder(),
                                                          hintText:
                                                              'Pilih ukuran',
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 18,
                                                      ),
                                                      TextField(
                                                        showCursor: true,
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              OutlineInputBorder(),
                                                          hintText:
                                                              'Harga barang persatuan',
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 18,
                                                      ),
                                                      TextField(
                                                        showCursor: true,
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              OutlineInputBorder(),
                                                          hintText:
                                                              'Harga barang semua',
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 125,
                                                      ),
                                                      FloatingActionButton
                                                          .extended(
                                                        onPressed: () {
                                                          Null;
                                                        },
                                                        label: const Text(
                                                            "Buat baru"),
                                                        icon: Icon(
                                                            CupertinoIcons
                                                                .plus),
                                                        backgroundColor:
                                                            Color(0xFFFD725A),
                                                        shape:
                                                            RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                        ),
                                                      )
                                                      // FloatingActionButton
                                                      //     .extended(
                                                      //   onPressed: () {
                                                      //     Null;
                                                      //   },
                                                      //   label: const Text(
                                                      //       "Buat Baru"),
                                                      //   icon: Icon(
                                                      //       CupertinoIcons
                                                      //           .plus),
                                                      //   backgroundColor:
                                                      //       Color.fromARGB(230,
                                                      //           241, 70, 73),
                                                      //   shape:
                                                      //       RoundedRectangleBorder(
                                                      //     borderRadius:
                                                      //         BorderRadius
                                                      //             .circular(10),
                                                      //   ),
                                                      // ),
                                                    ],
                                                  )),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                    label: const Text("Tambah pesanan"),
                                    icon: Icon(CupertinoIcons.plus),
                                    backgroundColor: Color(0xFFFD725A),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Total Harga",
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal,
                                    letterSpacing: 1,
                                    color: Colors.black.withOpacity(0.9),
                                  ),
                                ),
                                Container(
                                    child: Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 30),
                                  child: FloatingActionButton.extended(
                                    onPressed: () {
                                      Null;
                                    },
                                    label: const Text("Beli"),
                                    icon: Icon(CupertinoIcons.cart),
                                    backgroundColor: Color(0xFFFD725A),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ),
                                )),
                              ],
                            ),
                          ),

                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.end,
                          //   children: [
                          //     Container(
                          //       padding: EdgeInsets.symmetric(
                          //           vertical: 5, horizontal: 30),
                          //       decoration: BoxDecoration(
                          //         color: Color(0xFFFD725A),
                          //         borderRadius: BorderRadius.circular(10),
                          //       ),
                          //       child: Text(
                          //         "Beli",
                          //         style: TextStyle(
                          //           fontSize: 17,
                          //           fontWeight: FontWeight.w600,
                          //           letterSpacing: 1,
                          //           color: Colors.white.withOpacity(0.9),
                          //         ),
                          //       ),
                          //     ),
                          //   ],
                          // ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
