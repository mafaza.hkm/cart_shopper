import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CartItemSamples extends StatelessWidget {
  List imgList = [
    "Nasi goreng",
    "Kopi",
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        for (int i = 0; i < imgList.length; i++)
          Container(
            height: 110,
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Checkbox(
                  activeColor: Color(0xFFFD725A),
                  value: true,
                  onChanged: (value) {},
                ),
                // Container(
                //   height: 70,
                //   width: 70,
                //   margin: EdgeInsets.only(right: 15),
                //   decoration: BoxDecoration(
                //     color: Color.fromARGB(255, 224, 224, 224),
                //     borderRadius: BorderRadius.circular(10),
                //   ),
                //   // child: Image.asset("images/${imgList[i]}.png"),
                // ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          imgList[i],
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.black.withOpacity(0.7),
                          ),
                        ),
                        Text(
                          "Rp10.000",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.black.withOpacity(0.6),
                          ),
                        ),
                        // Text(
                        //   "Tulis Catatan",
                        //   style: TextStyle(
                        //     fontSize: 14,
                        //     fontWeight: FontWeight.bold,
                        //     color: Colors.black.withOpacity(0.6),
                        //   ),
                        // ),
                        SizedBox(width: 5),
                        Text(
                          "Tulis Catatan",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.redAccent.withOpacity(0.6),
                          ),
                        )
                      ]),
                ),
                Spacer(),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // Icon(
                      //   Icons.delete,
                      //   color: Colors.redAccent,
                      // ),
                      Row(
                        children: [
                          Container(
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              color: Color(0xFFF7F8FA),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Icon(
                              CupertinoIcons.delete,
                              size: 18,
                              color: Colors.redAccent,
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                Null;
                              },
                              icon: Icon(
                                CupertinoIcons.minus,
                                size: 18,
                                color: Colors.redAccent,
                              )),
                          SizedBox(
                            height: 80,
                          ),
                          Text(
                            "01",
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          IconButton(
                              onPressed: () {
                                Null;
                              },
                              icon: Icon(
                                CupertinoIcons.plus,
                                size: 18,
                                color: Colors.redAccent,
                              ))
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
      ],
    );
  }
}
